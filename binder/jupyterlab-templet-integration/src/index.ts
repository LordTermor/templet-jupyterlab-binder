
import {
  JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import '../style/index.css';
import {
  IDocumentManager
} from '@jupyterlab/docmanager';
import {
  ICommandPalette
} from '@jupyterlab/apputils';
/**
 * Initialization data for the Templet Integration extension.
 */
const extension: JupyterLabPlugin<void> = {
  id: 'Templet Integration',
  autoStart: true,
  requires: [ICommandPalette, IDocumentManager],
  activate: (app: JupyterLab, palette: ICommandPalette,docmanager: IDocumentManager) => {
      var fs = require('fs');
    console.log(fs);
    const command: string = 'templet:design';
    app.commands.addCommand(command, {
        label: 'Design',
        execute: () => {
        const context = docmanager.contextForWidget(app.shell.currentWidget);
        fs.readFileSync(context.localPath,'utf8');       
        context.revert();
        }
    });
    app.commands.addKeyBinding({
        selector: 'body',
        keys: ['Shift D'],
        command: 'templet:design'
    });
    palette.addItem({command, category: 'Templet'});

  }
};

export default extension;
